# Dormitory room management
The project is designed for the administrators of the Transilvania University Dormitories. It helps them to set and to keep the evidence of:
- the number of rooms available for students in every dorm 
- the number of rooms not available for students
- the capacity of the rooms
- the type of the room (room just boys or just girls)
- how many places are assigned in each room for every faculty


![](src/Images/1.PNG)

## Functionalities
Using a user-friendly interface, the users can modifiy all the data available in the page, in the right side having acces to an overview of the assigned places with dynamically updated information.

![](src/Images/7.PNG)

For example, for a new academic year, the number of places alocated for the faculties won't be defined, so the forth column of every row will contain the "Neatribuita" ("Unassigned") message.

In order to add one or more faculties to a room the users have to go through 3 steps:
- Make sure the room has the green checked icon with the message "Disponibil" ("Available")
- Add the number of available places in the room (the third column)
- Press the "Adauga facultate" ("Add faculty") button and choose the number of students and the faculty

![](src/Images/6.PNG)

This option is usually used when a room should have students from more than one faculty.

Otherwise, the third step can be completed selecting the checkbox for every row (or "Selecteaza toate camerele" which means "Select all the rooms") and then using the first dropdown from the blue header. A faculty can be chose and assigned to every selected room by pressing the green "Salveaza modificari" ("Save changes") button.

Also, the dorm management from the previous years can be viewed, the one from the previous year can be imported.

## Installation

Install dependencies
```bash
  npm install
```
Run the app
```bash
  npm start
```


