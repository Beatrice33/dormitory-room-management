/**
 *  This method returns the name of the user who is logged in to the intranet.
 *  The method detects if the running environment is development, in which case it returns a hardcoded mail, and in production takes over the mail from DNN
 * @method
 * @returns {string|*}
 */
export const getUsername = () => {
  if (!process.env.NODE_ENV || process.env.NODE_ENV === 'development') {
    //// on this branch is the code that is executed in the development environment. It is hardcoded to return a administrator email
    return "tinca.felicia@unitbv.ro";
    // return "paul.mirela@unitbv.ro"
    //return "maican@unitbv.ro";
  } else {
    // the branch for the production environment that takes the value from the html tag that extracts the username from DNN. The tag is in index.html
    let element = document.getElementById("userid");
    return element.value;
  }
};
