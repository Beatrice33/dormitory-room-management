import axios from "../../axios-AGSIS-API";

export const getCurrentAcademicYear = () => {
    axios
        .get("api/AnUniversitar/AnUniversitarCurent")
        .then(year => {
            return {
                    label: year.data.Denumire,
                    value: year.data.ID_AnUniv
                };
            }
        )
};

export const getStudentId = (studentEmail, academicYearID) => {
    axios
        .get("api/Student/ListStudentByUsernameAnUniv?username=" + studentEmail + "&id_anUniversitar=" + academicYearID)
        .then(student => {
                return student.ID_Student;
            }
        );
};

export const getStudentById = (idStudent, idAnUniv) => {
    axios.get("api/Student/StudentGetByAnUniv?iD_Student=" + idStudent + "&ID_AnUniv=" + idAnUniv)
        .then(st => {
                return st.data;
            }
        )
};

export const getPreaccommodationStartEndDates = (idAnUniv) => {
    axios
        .get("api/AnUniversitar/SetariCamineListByAnUniv?ID_AnUniv=" + idAnUniv)
        .then(settings => {
                let startEndDates = {
                    startDate: null,
                    endDate: null
                };
                for (let index in settings.data) {
                    if (settings.data.hasOwnProperty(index)) {
                        if (settings.data[index].NumeSetare === "CamineDataStartPrecazare") {
                            startEndDates.startDate = settings.data[index].ValoareSetare;
                        }
                        if (settings.data[index].NumeSetare === "CamineDataSfarsitPrecazare") {
                            startEndDates.endDate = settings.data[index].ValoareSetare;
                        }
                    }
                }
            }
        );
};

export const  getDormsByIdYear = (idAnUniv) => {
    axios
        .get("api/Camin/CaminListByAnUniv?ID_AnUniv=" + idAnUniv)
        .then(dormitory => {
            let dormitoryList = [];
            for (var i = 0; i < dormitory.data.length; i++) {
                dormitoryList.push({
                    label: dormitory.data[i].DenumireCamin,
                    value: dormitory.data[i].ID_Camin
                });
            }
            console.log();
            return dormitoryList;
        })
        .catch(error => {
            console.log(error);
        });
};

export const getRoomListByIdCamin = (idCamin) => {
    axios
        .get("api/Camera/CameraListByCamin?iD_Camin=" + idCamin)
        .then(rooms => {
            let roomList = [];
            for (var i = 0; i < rooms.data.length; i++) {
                roomList.push({
                    label: rooms.data[i].NumarCamera,
                    value: rooms.data[i].ID_Camera
                });
            }
            return roomList;
        })
        .catch(error => {
            console.log(error);
        });
};