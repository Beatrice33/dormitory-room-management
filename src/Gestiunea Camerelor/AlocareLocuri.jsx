import React, { Component } from "react";
import axios from "../axios-AGSIS-API";
import Select from "react-select";
import CreatableSelect from "react-select/creatable/dist/react-select.esm";
import "./AlocareLocuri.css";
import {
  Button,
  Modal,
  Icon,
  Input,
  Menu,
  Dropdown,
  Card,
  Segment, Popup,
} from "semantic-ui-react";
import { getUsername } from "../services/GetUsername";
import CameraTabel from "../Components/CameraTabel";

class AlocareLocuri extends Component {
  state = {
    loaded: false,
    listaAniUniversitari: [],
    detaliiCamere: [],
    listaFacultati: [],
    listaCamine: [],
    caminSelectat: null,
    idCamin: null,
    selectedOption: null,
    listaCamere: null,
    anUniversitarPrecedent: null,
    listaEtaje: [],
    listaTipCamera:[{key:0, value:"Camere Pare", label: "Camere Pare"}, {key:1, value:"Camere Impare", label: "Camere Impare"}],
    etajSelectat: [],
    anUniversitarCurent: null,
    anCurentSiPrecedent: [],
    username: null,
    idfacultate: null,
    ok: true,
    open: false,
    open1: false,
    open2: false,
    etajCurent: null,
    etajCamera: null,
    idCamera: "",
    nrLocuri: null,
    checked: false,
    checkedRadio: false,
    idCheckAll: [],
    selectedOption1: [],
    totalCamere: 0,
    listaCamereChecked: [],
    messageIsOpen:false,
    facultateCamera: []
  };

  handleOpen = () => this.setState({ open: true });
  handleClose = () => this.setState({ open: false });

  handleOpen1 = () => this.setState({ open1: true });
  handleClose1 = () => this.setState({ open1: false });

  handleOpen2 = () => this.setState({ open2: true });
  handleClose2 = () => this.setState({ open2: false });

  getInfoGestiune = (idAnuniv) => {
    const username = getUsername();
    this.setState({ username: username });

    axios
      .get("Camin/CaminListByAnUniv?ID_AnUniv=" + idAnuniv)
      .then((camine) => {
        axios
          .get(
            "Camin/CaminListByUsernameAdministrator?UsernameAdministrator=" +
              username
          )
          .then((admin) => {
            let lista = [];
            camine.data.forEach((camin) => {
              admin.data.forEach((adminCamin, index) => {
                if (
                  camin.ID_Camin === adminCamin.ID_Camin &&
                  camin.Status === 200
                ) {
                  let caminNou = {
                    key: index,
                    label: camin.DenumireCamin,
                    value: camin.ID_Camin,
                  };
                  lista.push(caminNou);
                }
              });
            });
            this.setState({ listaCamine: lista });

            if (lista.length > 0) {
              this.setState({
                caminSelectat: lista[0].value,
                denumireCaminSelectat: lista[0].label
              }); //caminSelectat=the first dorm from the list (if necessary)

              axios
                .get(
                  "Camera/CameraCuFacultatiAlocateListByCaminAnUniv?iD_Camin=" +
                    lista[0].value +
                    "&iD_AnUniv=" +
                    idAnuniv
                )
                .then((camere) => {
                  this.setState({
                    detaliiCamere: camere.data.sort((camera1, camera2) =>
                      camera1.NumarCamera < camera2.NumarCamera ? -1 : 1
                    ),
                    detaliiCamereCopy: camere.data.sort((camera1, camera2) =>
                        camera1.NumarCamera < camera2.NumarCamera ? -1 : 1
                    ),
                  });
                });
              axios
                .get(
                  "Camin/CaminEtajListByCamin?iD_Camin=" +
                    lista[0].value +
                    "&iD_AnUniv=" +
                    idAnuniv
                )
                .then((etaje) => {
                  let temp = [];
                  etaje.data.forEach((et, index) => {
                    let etaj = {
                      key: index,
                      value: index,
                      label: "Etajul " + index,
                    };
                    temp.push(etaj);
                  });
                  this.setState({ listaEtaje: temp });
                });
            }
            axios
              .get(
                "FacultateCamera/FacultateCameraRaportLocuriByCaminAnUniv?ID_Camin=" +
                  this.state.caminSelectat +
                  "&ID_AnUniv=" +
                  idAnuniv
              )
              .then((response) => {
                let lista = [];
                response.data.forEach(item => {
                  let raport = {
                    ID_Facultate: item.ID_Facultate,
                    DenumireFacultate: item.DenumireFacultate,
                    NrLocuriAlocateFacultate:
                      item.NrTotalLocuriAlocateFacultate,
                  };
                  lista.push(raport);
                });
                this.setState({ raportLocuri: lista });
              });
          });
      });
    axios
      .get("AnUniversitar/AnUniversitar_GetCurrentAndPrevious")
      .then((ani) => {
        let lista = [];
        ani.data.forEach((aniUniversitari, index) => {
          if (
            idAnuniv === aniUniversitari.ID_AnUniv ||
            this.state.anUniversitarPrecedent === aniUniversitari.ID_AnUniv
          ) {
            let aniNoi = {
              key: index,
              value: aniUniversitari.ID_AnUniv,
              label: aniUniversitari.Denumire,
            };
            lista.push(aniNoi);
          }
        });
        this.setState({ anCurentSiPrecedent: lista });
      });
  };

  componentDidMount() {
    document.addEventListener('scroll', () => {
      const isTop = window.scrollY > 100;
      if (isTop !== this.state.isTop) {
        this.setState({ isTop })
      }
    });

    axios
        .get("AnUniversitar/AnUniversitarCurent").then((anUniv) => {
         this.setState({
        anUniversitarCurent: anUniv.data.ID_AnUniv,
        anUniversitarPrecedent: anUniv.data.Id_AnUnivPrecedent, //used to memorize info about the previous year
        anSelectat: anUniv.data.ID_AnUniv,
        denumireAnSelectat: anUniv.data.Denumire,
         });

      axios
          .get("AnUniversitar/AnUniversitar_GetCurrentAndPrevious")
          .then((aniUniversit) => {
            let lista = [];
            aniUniversit.data.forEach((anUniv) => {
              let an = {
                label: anUniv.Denumire,
                value: anUniv.ID_AnUniv,
              };
              if (an.value <= this.state.anSelectat + 1) lista.push(an);
            });
            lista.sort((a, b) => (a.text < b.text ? 1 : -1));
            this.setState({
              listaAniUniversitari: lista,
            });
          });
      axios.get("Facultate/FacultateList").then((response) => {
        let listaFacultati = [];
        for (let facultate of response.data) {
          listaFacultati.push({
            key: facultate.ID_Facultate,
            value: facultate.Denumire,
            label: facultate.Denumire,
          });
        }
        this.setState({listaFacultati: listaFacultati});
      });

      this.getInfoGestiune(anUniv.data.ID_AnUniv);
    });
  }



  alegeAnUniversitar = (an) => {
    this.setState({
      anSelectat: an.value,
      denumireAnSelectat: an.label,
      detaliiCamere: [],
      listaCamine: [],
      caminSelectat: null,
      idCamin: null,
      selectedOption: null,
      listaCamere: null,
      anUniversitarPrecedent: null,
      listaEtaje: [],
      etajSelectat: [],
      anUniversitarCurent: null,
      anCurentSiPrecedent: [],
      username: null,
      idfacultate: null,
      ok: true,
      open: false,
      open1: false,
      open2: false,
      etajCurent: null,
      etajCamera: null,
      idCamera: "",
      nrLocuri: null,
      checked: false,
      checkedRadio: false,
      idCheckAll: [],
      selectedOption1: [],
      totalCamere: 0,
      listaCamereChecked: [],
      facultateCamera: []
    });
    this.getInfoGestiune(an.value);
  };

  alegeFacultatea = (selectedOption) => {
    const selectedOption1 = selectedOption.key;
    this.setState({ idfacultate: selectedOption1 });
    if (this.state.camereChecked !== undefined) {
      this.state.camereChecked.map(camera => camera.ID_Facultate = selectedOption1);
    }
  };

  alegeEtajul = (etajSelectat) => {
    //filter for the floor
    const etajSelectat1 = etajSelectat.value;
    this.setState({
      etajCurent: etajSelectat1,
      checkedRadio: false });

    this.uncheckRows(this.state.camereChecked)
  };

  alegeTipCamera=(tipCameraSelectat)=> {
    if (tipCameraSelectat!==null) {
      let temp = [];

      this.state.detaliiCamere
              .map(item=>
          (tipCameraSelectat.key === 0) ?
              (item.NumarCamera % 2 === 0) &&
                  temp.push( {
                    ID_Camera: item.ID_Camera,
                    ID_AnUniv: item.ID_AnUniv,
                    NumarCamera: item.NumarCamera,
                    TipCameraMF: item.Tilde,
                    Etaj: item.Etaj,
                    Disponibila: item.Disponibila,
                    FacultatiCamera: item.FacultatiCamera,
                    ID_Camin: item.ID_Camin,
                    NrLocuri: item.NrLocuri
                  })
              :
              (item.NumarCamera % 2 !== 0) &&
              temp.push( {
                ID_Camera: item.ID_Camera,
                ID_AnUniv: item.ID_AnUniv,
                NumarCamera: item.NumarCamera,
                TipCameraMF: item.Tilde,
                Etaj: item.Etaj,
                Disponibila: item.Disponibila,
                FacultatiCamera: item.FacultatiCamera,
                ID_Camin: item.ID_Camin,
                NrLocuri: item.NrLocuri
              })
              )
        this.setState({detaliiCamere: temp})
      }
    else{
      let listaDetaliiCamere=[...this.state.detaliiCamereCopy]
      this.setState({detaliiCamere:listaDetaliiCamere})
    }
  }

  handleCheckbox = () => {
    // id-isChecked, value-idCamera,name-nrLocuri
    const checkboxes = document.getElementsByClassName("checkbox");
    let lista = [];
    let listaChecked = [];
    for (let i = 0; i < checkboxes.length; i++) {
      if (checkboxes[i].checked) {
        lista = {
          ID_Camera: checkboxes[i].value,
          ID_Facultate: this.state.idfacultate,
          NrLocuriAlocateFacultate: checkboxes[i].name,
          ID_AnUniv: this.state.anSelectat,
        };
        listaChecked.push(lista);
      }
    }
    this.setState({ camereChecked: listaChecked });

  };

  actualizareRaport = () => {
    axios
      .get(
        "FacultateCamera/FacultateCameraRaportLocuriByCaminAnUniv?ID_Camin=" +
          this.state.caminSelectat +
          "&ID_AnUniv=" +
          this.state.anSelectat
      )
      .then((response) => {
        let lista = [];
        response.data.forEach(item => {
          let raport = {
            ID_Facultate: item.ID_Facultate,
            DenumireFacultate: item.DenumireFacultate,
            NrLocuriAlocateFacultate: item.NrTotalLocuriAlocateFacultate,
          };
          lista.push(raport);
        });
        this.setState({ raportLocuri: lista });
      });
  };

  salveazaFacultati = () => {
    //add faculty/room for all the students from this room
    axios
      .post(
        "FacultateCamera/FacultateCameraMergeArray",
        this.state.camereChecked
      )
      .then((response) => {
        if (response.data.camereActualizate.length > 0) {
          let cameretemp = [...this.state.detaliiCamere];
          let camereActualizate = [...response.data.camereActualizate];
          let count = 0;
          for (let item in cameretemp) {
            for (let k in camereActualizate) {
              if (
                cameretemp[item].ID_Camera === camereActualizate[k].ID_Camera
              ) {
                for (let j in cameretemp[item].FacultatiCamera) {
                  if (cameretemp[item].FacultatiCamera.hasOwnProperty(j)) {
                    if (cameretemp[item].FacultatiCamera[j].ID_Facultate === camereActualizate[k].ID_Facultate) {
                      cameretemp[item].FacultatiCamera[j].NrLocuriAlocateFacultate = camereActualizate[k].NrLocuriAlocateFacultate;
                      count++;
                      break;
                    }
                  }
                }
                if (count === 1) {
                  break;
                }
                if (count === 0) {
                  cameretemp[item].FacultatiCamera.push(camereActualizate[k]);
                }
              }
            }
          }
          this.setState({ detaliiCamere: cameretemp });
          this.actualizareRaport();
        }
        this.uncheckRows(this.state.camereChecked)
      })
      .catch((error) => {
        alert("a aparut o eroare" + error);
      });
  };

  adaugaCamera = () => {
    // 2 API calls needed to add a room (one for general info about the room, another one for the capacity of the room)

    let verificaDate=true
    let camere = this.state.detaliiCamere;
    for (let i = 0; i < camere.length; i++) {
      if (camere[i].NumarCamera === this.state.numeCamera) {
        this.setState({ok:false})
        verificaDate=false
        alert("Exista deja o camera cu acest nume.");
        break;
      }
      else this.setState({ok:true})
    }

    if (verificaDate) {
      this.setState({ facultateCamera: [], detaliiCamera: [] });
  //    axios.post("Camera/CameraAdd", camera).then(() => {
        //for the moment this functionality should not post any data
    //  });
    }
  };

  checkAll = (event) => {
    const checkboxes = document.getElementsByClassName("checkbox");
    let listaChecked = [];
    let lista = {};
    this.setState({ camereChecked: [] });
    for (let i = 0; i < checkboxes.length; i++) {
      if (event.target.checked) {
        if (checkboxes[i].checked === false || checkboxes[i].checked === true) {
          lista = {
            ID_Camera: checkboxes[i].value,
            ID_Facultate: this.state.idfacultate,
            NrLocuriAlocateFacultate: checkboxes[i].name,
            ID_AnUniv: this.state.anSelectat,
          };
          listaChecked.push(lista);
          checkboxes[i].checked = true;
        }
      }
      if (event.target.checked === false) {
        if (checkboxes[i].checked === true) checkboxes[i].checked = false;
        this.state.camereChecked.splice(0, this.state.camereChecked.length);
      }
    }
    this.setState({ camereChecked: listaChecked });
  };

  selectieCamin = (selectieNoua) => {
    // when another dorm is chosen, all the data is updated for new values
    this.setState({
      detaliiCamere: [],
      listaEtaje: [],
      raportLocuri: [],
      etajCurent: null,
      caminSelectat: selectieNoua.value,
      denumireCaminSelectat:selectieNoua.label,
      checkedRadio: false
    });

    this.uncheckRows(this.state.camereChecked)
    axios
      .get(
        "Camera/CameraCuFacultatiAlocateListByCaminAnUniv?iD_Camin=" +
          selectieNoua.value +
          "&iD_AnUniv=" +
          this.state.anSelectat
      )
      .then((camere) => {
        this.setState({
          detaliiCamere: camere.data.sort((camera1, camera2) =>
            camera1.NumarCamera < camera2.NumarCamera ? -1 : 1
          ),
        });
      });
    axios
      .get(
        "Camin/CaminEtajListByCamin?iD_Camin=" +
          selectieNoua.value +
          "&iD_AnUniv=" +
          this.state.anSelectat
      )
      .then((etaje) => {
        let temp = [];
        etaje.data.forEach((et, index) => {
          let etaj = {
            key: index,
            value: index,
            label: "Etajul " + index,
          };
          temp.push(etaj);
        });
        this.setState({ listaEtaje: temp });
      });
    axios
      .get(
        "FacultateCamera/FacultateCameraRaportLocuriByCaminAnUniv?ID_Camin=" +
          selectieNoua.value +
          "&ID_AnUniv=" +
          this.state.anSelectat
      )
      .then((response) => {
        let lista = [];
        response.data.forEach(item => {
          let raport = {
            ID_Facultate: item.ID_Facultate,
            DenumireFacultate: item.DenumireFacultate,
            NrLocuriAlocateFacultate: item.NrTotalLocuriAlocateFacultate,
          };
          lista.push(raport);
        });
        this.setState({ raportLocuri: lista });
      });
  };

  returneazaDenumireCamin = (idCamin) => {
    //returns the name of the dorm based on ID
    let nume = null;
    this.state.listaCamine.forEach((item) => {
      if (item.value === idCamin) nume = item.label;
    });
    return nume;
  };

  returneazaDenumireAnUniversitar = (idAnUniv) => {
    //returns the name of the yeaar based on ID
    let nume = null;
    this.state.anCurentSiPrecedent.forEach((item) => {
      if (item.value === idAnUniv) nume = item.label;
    });
    return nume;
  };

  afisareCamere = () => {
    //it reloads all the rooms (if a filter was chosen previously)
    this.setState({ etajCurent: null });

    this.uncheckRows(this.state.camereChecked)

      this.state.detaliiCamere
        .filter(camere =>
          this.state.etajCurent === null
            ? true
            : camere.Etaj === this.state.etajCurent
        )
        .map(item=> {
          return (
            <CameraTabel
              key={item.ID_Camera}
              ID_Camera={item.ID_Camera}
              NumarCamera={item.NumarCamera}
              NrLocuri={item.NrLocuri}
              TipCameraMF={item.TipCameraMF}
              Disponibila={item.Disponibila}
              raportLocuri={this.state.raportLocuri}
              FacultatiCamera={item.FacultatiCamera}
              anUniversitarCurent={this.state.anSelectat}
              idCamin={this.state.idCamin}
              caminSelectat={this.state.caminSelectat}
              ID_Camin={this.state.caminSelectat}
              listaFacultati={this.state.listaFacultati}
              handleCheckbox={this.handleCheckbox}
              actualizareNumeCamera={(ID_Camera, numecamera) => {
                this.actualizareNumeCamera(ID_Camera, numecamera);
              }}
              actualizareLocuri={(ID_Camera, locuricamera) => {
                this.actualizareLocuri(ID_Camera, locuricamera);
              }}
              actualizareGenCamera={(ID_Camera, gen) => {
                this.actualizareGenCamera(ID_Camera, gen);
              }}
              actualizareFacultateCamera={(
                ID_Camera,
                facultatecamera,
                denumire,
                idfacultatecamera,
                idFacultate
              ) => {
                this.actualizareFacultateCamera(
                  ID_Camera,
                  facultatecamera,
                  denumire,
                  idfacultatecamera,
                  idFacultate
                );
              }}
              actualizareFacultateDelete={(ID_Camera, ID_FacultateCamera) => {
                this.actualizareFacultateDelete(ID_Camera, ID_FacultateCamera);
              }}
              actualizareDisponibilitate={(ID_Camera, disponibil) => {
                this.actualizareDisponibilitate(ID_Camera, disponibil);
              }}
            />
          );
        });

    this.setState({ checkedRadio: true });
  };


  importNrLocuri = () => {
    //import the previous management of the number of places allocated for students in the rooms
    this.setState({ detaliiCamere: [] });
    axios
      .post(
        "Camera/LocuriCameraImportDinAltAnUniv?ID_AnUnivDinCareSeImporta=" +
          this.state.anUniversitarPrecedent +
          "&ID_AnUnivInCareSeImporta=" +
          this.state.anSelectat +
          "&ID_Camin=" +
          this.state.caminSelectat
      )
      .then(() => {
        this.importDateCamere(this.state.caminSelectat,this.state.anSelectat)
      })
      .catch((error) => {
        alert("a aparut o eroare" + error);
      });

    this.uncheckRows(this.state.camereChecked)
    this.setState({ open1: false });
  };

  uncheckRows=(row)=>{
    //it unchecks every row that has been checked when another dorm is selected
    // or when the data from a previous year is expected to be displayed
    const checkboxes = document.querySelectorAll("input[type=checkbox]");
    if (row !== undefined) {
      let cameretemp = [...row];
      for (let i in checkboxes) {
        if (checkboxes.hasOwnProperty(i)) {
          if (checkboxes[i].checked) {
            checkboxes[i].checked = false;
            cameretemp.splice(checkboxes[i], 1);
          }
        }
      }
      this.setState({ camereChecked: cameretemp });
    }
  }

  importDateCamere=(camin, anUniversitar)=>{
    axios
        .get(
            "Camera/CameraCuFacultatiAlocateListByCaminAnUniv?iD_Camin=" +
            camin +
            "&iD_AnUniv=" +
            anUniversitar
        )
        .then((camere) => {
          this.setState({
            detaliiCamere: camere.data.sort((camera1, camera2) =>
                camera1.NumarCamera < camera2.NumarCamera ? -1 : 1
            ),
          });
        });
    this.actualizareRaport()
  }

  importFacultateCamera = () => {
    //import the previous management of the faculties allocated for every room from a previous year
    this.setState({ detaliiCamere: [] });
    axios
      .post(
        "FacultateCamera/FacultateCameraImportDinAltAnUniv?ID_AnUnivDinCareSeImporta=" +
          this.state.anUniversitarPrecedent +
          "&ID_AnUnivInCareSeImporta=" +
          this.state.anSelectat +
          "&ID_Camin=" +
          this.state.caminSelectat
      )
      .then(() => {
        this.importDateCamere(this.state.caminSelectat,this.state.anSelectat)
      })
      .catch((error) => {
        alert("a aparut o eroare" + error);
      });

    this.uncheckRows(this.state.camereChecked)
    this.setState({ open2: false });
  };

  actualizareNumeCamera = (ID_Camera, numecamera) => {
    //it is used to update the name of the room
    let cameretemp = [...this.state.detaliiCamere];
    for (let item in cameretemp) {
      if (cameretemp[item].ID_Camera === ID_Camera) {
        cameretemp[item].NumarCamera = numecamera;
        break;
      }
    }
    cameretemp.sort((camera1, camera2) =>
      camera1.NumarCamera < camera2.NumarCamera ? -1 : 1
    );

    this.setState({ detaliiCamere: cameretemp });
  };

  actualizareDisponibilitate = (ID_Camera, disponibil) => {
    let cameretemp = [...this.state.detaliiCamere];
    for (let item in cameretemp) {
      if (cameretemp[item].ID_Camera === ID_Camera) {
        cameretemp[item].Disponibila = disponibil;
        break;
      }
    }
    this.setState({ detaliiCamere: cameretemp });
  };

  actualizareLocuri = (ID_Camera, numarLocuri) => {
    let cameretemp = [...this.state.detaliiCamere];
    for (let item in cameretemp) {
      if (cameretemp[item].ID_Camera === ID_Camera) {
        cameretemp[item].NrLocuri = numarLocuri;
        break;
      }
    }
    this.setState({ detaliiCamere: cameretemp });
  };

  actualizareGenCamera = (ID_Camera, gen) => {
    let cameretemp = [...this.state.detaliiCamere];
    for (let item in cameretemp) {
      if (cameretemp[item].ID_Camera === ID_Camera) {
        cameretemp[item].TipCameraMF = gen;
        break;
      }
    }
    this.setState({ detaliiCamere: cameretemp });
  };

  actualizareFacultateCamera = (ID_Camera, facultatecamera, denumire, idfacultatecamera, idFacultate) => {
    let cameretemp = [...this.state.detaliiCamere]; //it checks if the faculty already exists to know which API should be called
    let count = 0;
    for (let item in cameretemp) {
      if (cameretemp[item].ID_Camera === ID_Camera) {
        for (let j in cameretemp[item].FacultatiCamera) {
          if (cameretemp[item].FacultatiCamera.hasOwnProperty(j)) {
            if (cameretemp[item].FacultatiCamera[j].ID_Facultate === idFacultate) {
              cameretemp[item].FacultatiCamera[j].NrLocuriAlocateFacultate =
                  facultatecamera.NrLocuriAlocateFacultate;
              cameretemp[item].FacultatiCamera[j].DenumireFacultate = denumire;
              count++;
              if (count > 0) {
                this.actualizareRaport()
              }
              break;
            }
          }
        }
        if (count === 0) {
          facultatecamera["DenumireFacultate"] = denumire;
          facultatecamera["ID_FacultateCamera"] = idfacultatecamera;
          facultatecamera["ID_Facultate"] = idFacultate;
          cameretemp[item].FacultatiCamera.push(facultatecamera);

          this.actualizareRaport()
          break;
        }
      }
    }
    this.setState({ detaliiCamere: cameretemp });
  };

  successMessage=()=>{
    this.setState({messageIsOpen:true})
  }

  actualizareFacultateDelete = (ID_Camera, ID_FacultateCamera) => {
    let cameretemp = [...this.state.detaliiCamere];
    for (let item in cameretemp) {
      if (cameretemp[item].ID_Camera === ID_Camera) {
        for (let j in cameretemp[item].FacultatiCamera) {
          if (cameretemp[item].FacultatiCamera.hasOwnProperty(j)) {
            if (
                cameretemp[item].FacultatiCamera[j].ID_FacultateCamera === ID_FacultateCamera) {
              cameretemp[item].FacultatiCamera[j].DenumireFacultate = null;
              cameretemp[item].FacultatiCamera[j].NrLocuriAlocateFacultate = 0;

              this.actualizareRaport()
              break;
            }
          }
        }
      }
    }
    this.setState({ detaliiCamere: cameretemp });
  };

  render() {
    const { open } = this.state;
    const { open1 } = this.state;
    const { open2 } = this.state;
    const {messageIsOpen}=this.state;
    let locuriTotal = 0;

    return (
      <div style={{width:"100%"}}>
        <Menu className="menuTop">
          <Menu.Item>Număr camere: {this.state.detaliiCamere.length}</Menu.Item>
          <Menu.Item>
            <Modal
              trigger={
                <Button
    type="button"
    color="teal"
    icon="add"
    content="Adaugă cameră"
    onClick={this.handleOpen}
    />
              }
              open={open}
              onOpen={this.handleOpen}
              onClose={this.handleClose}
              size="small"
            >
              <Modal.Header
                icon="right arrow"
                content="Completați informațiile"
              />
              <Modal.Content>
                Număr camera:{" "}
                <Input
                  focus
                  placeholder=""
                  onChange={(event) =>
                    this.setState({ numeCamera: event.target.value })
                  }
                />
                <p/>
                Locuri disponible (facultativ):{" "}
                <Input
                  type="number"
                  focus
                  placeholder=" "
                  onChange={(event) =>
                    this.setState({ nrLocuri: event.target.value })
                  }
                />
                <p/>
              </Modal.Content>
              <Modal.Actions>
                <Button
                  type="button"
                  color="red"
                  content="Renunță"
                  onClick={this.handleClose}
                />
                {this.state.ok? (
                  <Modal
                    size="tiny"
                    trigger={
                      <Button
                        type="button"
                        color="blue"
                        onClick={this.adaugaCamera}
                      >
                        <Icon name="checkmark" /> Adaugă
                      </Button>
                    }
                  >
                    <Modal.Header>
                      {" "}
                      Camera adaugata <Icon name="check square" color="green" />
                    </Modal.Header>
                    <Modal.Content>
                      <p>
                        {" "}
                        Ați adaugat cu succes camera cu numărul{" "}
                        {this.state.numeCamera}!
                      </p>
                    </Modal.Content>
                    <Modal.Actions>
                      <Button
                        type="button"
                        color="green"
                        content="Inchide"
                        onClick={this.handleClose}
                      />
                    </Modal.Actions>
                  </Modal>
                ) : (
                  <Modal
                    size="tiny"
                    trigger={
                      <Button
                        type="button"
                        color="blue"
                        onClick={this.adaugaCamera}
                      >
                        <Icon name="checkmark" /> Adauga
                      </Button>
                    }
                  >
                    <Modal.Header>
                      {" "}
                      Camera nu a putut fi adaugata{" "}
                      <Icon name="close" color="red" />
                    </Modal.Header>
                    <Modal.Content>
                      Există deja o camera cu acest nume.
                    </Modal.Content>{" "}
                  </Modal>
                )}
              </Modal.Actions>
            </Modal>
          </Menu.Item>
          {this.state.listaCamine.length > 1 && (
            <Select
              className="d1"
              fluid
              selection
              placeholder={this.state.denumireCaminSelectat}
              options={this.state.listaCamine}
              onChange={this.selectieCamin}
              value={this.state.caminSelectat}
            />
          )}
          <Select
            className="d1"
            fluid
            selection
            placeholder={this.state.denumireAnSelectat}
            options={this.state.listaAniUniversitari}
            onChange={this.alegeAnUniversitar}
            value={this.state.anSelectat}
          />
          <Menu.Item className="right" >
            <Dropdown
              style={{color:"darkblue"}}
              button
              labeled
              floating
              text={"Gestiunea anterioară"}
            >
              <Dropdown.Menu>
                <Dropdown.Header
                  icon="tags"
                  content="Import date an precedent"
                />
                <Modal
                  trigger={
                    <Dropdown.Item>
                      Numărul de locuri alocat facultăților
                    </Dropdown.Item>
                  }
                  open={open1}
                  onOpen={this.handleOpen1}
                  onClose={this.handleClose1}
                  size="tiny"
                >
                  <Modal.Header content="Import numar locuri an precedent" />
                  <Modal.Content>
                    Această operație va importa din anul anterior distribuția
                    locurilor din camera.
                  </Modal.Content>
                  <Modal.Actions>
                    <Button
                      type="button"
                      color="green"
                      content="De acord"
                      onClick={this.importNrLocuri}
                    />
                  </Modal.Actions>
                </Modal>
                <Modal
                  trigger={
                    <Dropdown.Item>
                      Distribuția facultăților pe camere
                    </Dropdown.Item>
                  }
                  open={open2}
                  onOpen={this.handleOpen2}
                  onClose={this.handleClose2}
                  size="tiny"
                >
                  <Modal.Header content="Import facultate-camera an precedent" />
                  <Modal.Content>
                    Această operație va importa din anul anterior distribuția
                    facultăților pe camere.
                  </Modal.Content>
                  <Modal.Actions>
                    <Button
                      type="button"
                      color="green"
                      content="De acord"
                      onClick={this.importFacultateCamera}
                    />
                  </Modal.Actions>
                </Modal>
              </Dropdown.Menu>
            </Dropdown>
          </Menu.Item>
        </Menu>
        <h1 style={{ textAlign: "center" }}>
          Gestiunea Camerelor{" "}
          {this.returneazaDenumireCamin(this.state.caminSelectat)}{" "}

        </h1>
        <h3 style={{ textAlign: "center" }}>
          {"-"} {this.returneazaDenumireAnUniversitar(this.state.anSelectat)}{" "}
          {"-"}
        </h3>
        <Menu
          pointing
          secondary
          style={{
            backgroundColor: "steelblue",
            borderRadius: "10px",
            width: "100%",
          }}
          className={this.state.isTop && "menu-fixed"}>
          <Menu.Item style={{width:"60%", marginLeft:"18px"}}>
            <span>ATRIBUIȚI CAMERELE SELECTATE UNEI SINGURE FACULTĂȚI:</span>
            <Select
              className="dropdownFacultate"
              placeholder="Alegeți facultatea"
              onChange={this.alegeFacultatea}
              options={this.state.listaFacultati}
            />
            <Button
              type="button"
              style={{ height: "40px", marginLeft: "30px" }}
              color="green"
              id="bttn"
              disabled={!this.state.camereChecked || !this.state.idfacultate}
              onClick={this.salveazaFacultati}
            >
              Salvați modificările
            </Button>
          </Menu.Item>
          <Menu.Item className="right" style={{width:"20%"}}>
          <CreatableSelect
              isClearable
              className="dropdownEtaj"
              placeholder="Tip camera"
              onChange={this.alegeTipCamera}
              options={this.state.listaTipCamera}
          />
          </Menu.Item>
          <Menu.Item className="right" style={{width:"20%"}}>
          <Select
              className="dropdownEtaj"
            placeholder="Etaj"
            onChange={this.alegeEtajul}
            options={this.state.listaEtaje}
          />
          </Menu.Item>
        </Menu>
        <Card
          style={{
            float: "right",
            display: "inline",
            position: "top",
            marginRight: "50px",
            height: "70%",
          }}
        >
          <Card.Content header="Evidență locuri alocate" />
          {this.state.raportLocuri !== undefined &&
            this.state.raportLocuri.map(item => {
              locuriTotal = locuriTotal + item.NrLocuriAlocateFacultate;
              return (
                <Card.Content key={item.ID_Facultate}>
                  <b>{item.DenumireFacultate}</b>:{" "}
                  {item.NrLocuriAlocateFacultate} locuri
                </Card.Content>
              );
            })}
          <Card.Content extra>
            {" "}
            <Icon name={"users"} color={"blue"} />
            Număr total locuri: {locuriTotal}
          </Card.Content>
        </Card>
        <Segment
          style={{ width: "72%", marginLeft: "2%", alignItems: "center" }}
        >
          <span
            style={{
              marginBottom: "8px",
              borderBottom: "1px solid grey",
              marginLeft: "4px",
              backgroundColor: "lightsteelblue",
              width: "19%",
              borderRadius: "7px",
              float: "left",
              textAlign: "center",
              clear: "left",
            }}
          >
            <input
              type="checkbox"
              className="checkAll"
              onChange={this.checkAll}
            />
            {". "} Selectează toate camerele{" "}
          </span>
          <span
            style={{
              borderBottom: "1px solid grey",
              backgroundColor: "lightsteelblue",
              width: "18%",
              borderRadius: "7px",
              alignItems: "center",
              display: "inline-block",
              float: "right",
              textAlign: "center",
            }}
          >
            <input
              type="radio"
              className="checkAll"
              checked={this.state.checkedRadio}
              onClick={this.afisareCamere}
            />
            {"`  "} Afișează toate camerele{" "}
          </span>
          <Popup
            content={'Modificarile au fost salvate cu succes!'}
            open={messageIsOpen}/>

          {this.state.camereChecked !== undefined ? (
            <span
              style={{
                marginLeft: "6px",
                marginTop: "8px",
                borderBottom: "2px solid #dd3131",
                width: "17%",
                borderRadius: "7px",
                alignItems: "center",
                float: "left",
                textAlign: "left",
                clear: "left",
              }}
            >
              {"  "}
              <Icon name={"info circle"} color={"blue"} />
              Camere selectate: {this.state.camereChecked.length}
            </span>
          ) : null}
          {this.state.detaliiCamere
            .filter(camere =>
              this.state.etajCurent == null
                ? true
                : camere.Etaj === this.state.etajCurent
            )
            .map(item => {
              return (
                <CameraTabel
                  key={item.ID_Camera}
                  ID_Camera={item.ID_Camera}
                  NumarCamera={item.NumarCamera}
                  NrLocuri={item.NrLocuri}
                  TipCameraMF={item.TipCameraMF}
                  Disponibila={item.Disponibila}
                  raportLocuri={this.state.raportLocuri}
                  FacultatiCamera={item.FacultatiCamera}
                  anUniversitarCurent={this.state.anSelectat}
                  idCamin={this.state.idCamin}
                  caminSelectat={this.state.caminSelectat}
                  ID_Camin={this.state.caminSelectat}
                  listaFacultati={this.state.listaFacultati}
                  handleCheckbox={this.handleCheckbox}
                  actualizareNumeCamera={(ID_Camera, numecamera) => {
                    this.actualizareNumeCamera(ID_Camera, numecamera);
                  }}
                  actualizareLocuri={(ID_Camera, locuricamera) => {
                    this.actualizareLocuri(ID_Camera, locuricamera);
                  }}
                  actualizareGenCamera={(ID_Camera, gen) => {
                    this.actualizareGenCamera(ID_Camera, gen);
                  }}
                  actualizareFacultateCamera={(
                    ID_Camera,
                    facultatecamera,
                    denumire,
                    idfacultatecamera,
                    idFacultate
                  ) => {
                    this.actualizareFacultateCamera(
                      ID_Camera,
                      facultatecamera,
                      denumire,
                      idfacultatecamera,
                      idFacultate
                    );
                  }}
                  actualizareFacultateDelete={(
                    ID_Camera,
                    ID_FacultateCamera
                  ) => {
                    this.actualizareFacultateDelete(
                      ID_Camera,
                      ID_FacultateCamera
                    );
                  }}
                  actualizareDisponibilitate={(ID_Camera, disponibil) => {
                    this.actualizareDisponibilitate(ID_Camera, disponibil);
                  }}
                  successMessage={() => {this.successMessage()}}/>
              );
            })}
        </Segment>
      </div>
    );
  }
}

export default AlocareLocuri;
