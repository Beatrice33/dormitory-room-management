import React, {Component} from 'react'
import '../Gestiunea Camerelor/AlocareLocuri.css'
import {Button, Dropdown, Table, Icon, Popup, Modal, Input, Grid} from "semantic-ui-react";
import 'semantic-ui-css/semantic.min.css'
import axios from "../axios-AGSIS-API";
import Select from "react-select"
import 'semantic-ui-css/semantic.css';

class CameraTabel extends Component {
    constructor(props) {
        super(props)
        this.state = {
            checked: false,
            on: false,
            fac: null,
            modalOpen: false,
            modalOpen1:false,
            popupOpen:false,
            selectedOption1: [],
            checkedItems: new Map()

        }
    }
    openPopup = () => this.setState({popupOpen: true})
    closePopup = () => this.setState({popupOpen: false})

    openModal = () => this.setState({modalOpen: true})
    closeModal = () => this.setState({modalOpen: true})


    schimbaGenulCamerei = (event) => {
        let gen = ""
        if (event.target.innerText === " Băieți") gen = "M"
        if (event.target.innerText === " Fete") gen = "F"
        if (event.target.innerText === " Nespecificat") gen = "N"
        const camera = {
            ID_Camera: this.props.ID_Camera,
            NrLocuri: this.props.NrLocuri,
            ID_AnUniv: this.props.anUniversitarCurent,
            TipCameraMF: gen
        }
        axios
            .post('Camera/LocuriCameraMerge', camera)
            .then(this.props.actualizareGenCamera(this.props.ID_Camera, camera.TipCameraMF))
            .catch(() => {
                alert("a aparut o eroare");

            })


    }

    disponibilitateCamera = (event) => {
        const disponibilitate = event.target.innerText
        let camera = {
            NumarCamera: this.props.NumarCamera,
            ID_Camera: this.props.ID_Camera,
            ID_Camin: this.props.ID_Camin,
            ID_AnUniv: this.props.anUniversitarCurent,
        }

        camera["Disponibila"] = disponibilitate !== "Indisponibil";

        axios
            .post('Camera/CameraUpdate', camera)
            .then(this.props.actualizareDisponibilitate(this.props.ID_Camera,camera.Disponibila))
            .catch(() => {
                alert("a aparut o eroare");
            })
    }

    modificaNumeCamera = () => {
        const camera = {
            NumarCamera: this.state.numarCameraNew,
            ID_Camera: this.props.ID_Camera,
            ID_Camin: this.props.ID_Camin,
            ID_AnUniv: this.props.anUniversitarCurent,
            Disponibila: this.props.Disponibila
        }
            this.state.numarCameraNew !== undefined && this.state.numarCameraNew !== " " && this.state.numarCameraNew != null ?
                axios
                    .post('Camera/CameraUpdate', camera)
                    .then(this.props.actualizareNumeCamera(this.props.ID_Camera,camera.NumarCamera) )
                    .catch(() => {
                        alert("a aparut o eroare");
                    }) : alert("Completarea campului 'Nume Camera' este obligatorie")
            this.setState({modalOpen: false})

    }


    alegeFacultatea = (selectedOption) => {
        const selectedOption1 = selectedOption.key
        const denumire=selectedOption.value
        this.setState({
            idFacultate: selectedOption1,// it memorizes the name of a faculty using its ID
            denumireFacultate:denumire
        })

    }

    facultateCameraAdd = () => {
        if (this.props.NrLocuri <= 0) alert("Adaugati locuri in camera inainte de atribuirea facultatilor.")
        if (this.props.NrLocuri > 0 & this.state.nrLocuriAlocate!==undefined && this.state.idFacultate!==undefined) {
            const facultateCamera = {
                ID_Camera: this.props.ID_Camera,
                ID_Facultate: this.state.idFacultate,
                ID_AnUniv: this.props.anUniversitarCurent,
                NrLocuriAlocateFacultate: this.state.nrLocuriAlocate
            }
            let sumaLocuri=0;
            for (let item in this.props.FacultatiCamera) {
                if (this.props.FacultatiCamera.hasOwnProperty(item)) {
                    if (this.props.FacultatiCamera[item].ID_Facultate === facultateCamera.ID_Facultate) {
                        sumaLocuri = parseInt(this.state.nrLocuriFacultatiCamera) - parseInt(this.props.FacultatiCamera[item].NrLocuriAlocateFacultate) + parseInt(facultateCamera.NrLocuriAlocateFacultate
                        )
                        break;
                    }
                }
            }
            if(sumaLocuri===0)  {sumaLocuri = parseInt(this.state.nrLocuriAlocate) + parseInt(this.state.nrLocuriFacultatiCamera)}
            if (sumaLocuri<=this.props.NrLocuri  ) {
                    axios
                        .post('FacultateCamera/FacultateCameraMerge', (facultateCamera))
                        .then(response=>{
                            this.props.actualizareFacultateCamera(this.props.ID_Camera,facultateCamera,this.state.denumireFacultate,response.data.camereActualizate[0].ID_FacultateCamera, this.state.idFacultate)
                            this.closePopup()
                            this.props.successMessage()
                        })
                        .catch(() => {
                            alert("a aparut o eroare");

                        })
            }
            if (sumaLocuri>this.props.NrLocuri) alert("Nu puteti adauga mai multe locuri decat ati setat in aceasta camera")
        }
        else alert("Completarea ambelor campuri(Facultate si numar locuri) este obligatorie!")
    }

    stergeFacultate = (ID_FacultateCamera) => {
        axios
            .post('FacultateCamera/FacultateCameraDelete?ID_FacultateCamera=' + ID_FacultateCamera)
            .then(this.props.actualizareFacultateDelete(this.props.ID_Camera,ID_FacultateCamera))

        this.setState({modalOpen1:false})
    }

    adaugaNrLocuri = (nrLocuriFacultatiCamera) => {
        if(this.state.nrLocuriCamera<nrLocuriFacultatiCamera){
            alert("Înainte de a reduce numărul de locuri din cameră verificați că ați modificat și numărul de locuri atribuit facultăților  deja existent din această camera.")
        }
        else {
            const locuriCamera = {
                ID_Camera: this.props.ID_Camera,
                NrLocuri: this.state.nrLocuriCamera,
                ID_AnUniv: this.props.anUniversitarCurent,
                TipCameraMF: this.props.TipCameraMF
            }
            if( this.props.TipCameraMF!=="M" && this.props.TipCameraMF!=="F") locuriCamera.TipCameraMF="N"
                this.state.nrLocuriCamera !== undefined ?
                    axios
                        .post('Camera/LocuriCameraMerge', locuriCamera)
                        .then(this.props.actualizareLocuri(this.props.ID_Camera,locuriCamera.NrLocuri))
                    : alert('Completarea campului pentru numarul de locuri este obligatorie!')
        }
    }
    render() {
        // const {modalOpen} = this.state
        // const {modalOpen1} = this.state
        const style = {
            width: "550px",
            height: "70px"
        }
        const style2 = {
            width: "450px",
            height: "70px"
        }
        const {popupOpen}=this.state
        let locuriTotal = 0;
        return (
            <div>
                <Table celled selectable className='tabelCamera'>
                    <Table.Header>
                        <Table.Row>
                            <Table.HeaderCell style={{width: "40px"}}>
                                {this.props.NrLocuri > 0  ?  // if the number of students from a room is not selected, checkbox will be disabled; this should be the first action
                                    <input type="checkbox" value={this.props.ID_Camera} className="checkbox" name={this.props.NrLocuri}
                                            onChange={this.props.handleCheckbox} /> :
                                    <input type="checkbox" className="checkbox" disabled={"disabled"}/>}
                            </Table.HeaderCell>
                            <Table.HeaderCell style={{width: "265px"}}>
                                <Modal open={this.state.modalOpen} onClose={this.closeModal} size='tiny'>
                                    <Modal.Header>Modificați numele camerei {this.props.NumarCamera}</Modal.Header>
                                    <Modal.Content>
                                        Nume camera:
                                        <Input focus onChange={(event) => this.setState({numarCameraNew: event.target.value})}/>
                                    </Modal.Content>
                                    <Modal.Actions>
                                        <Button
                                            type="button"
                                            color={'red'}
                                            onClick={this.closeModal}
                                        >
                                            Renunțați
                                        </Button>
                                        <Button
                                            type="button"
                                            color='green' onClick={this.modificaNumeCamera}>
                                            <Icon name='checkmark'/> Salvați modificările
                                        </Button>
                                    </Modal.Actions>
                                </Modal>
                                <Popup content={'Editați numele camerei'}
                                       trigger={<Icon style={{transform: "scale(1.3)", paddingBottom: "22px"}} color='teal' name='edit'
                                                      onClick={this.openModal}/>}/>
                                {" "} Camera:{this.props.NumarCamera}
                                <Dropdown icon={<Icon name={(this.props.TipCameraMF === "F") ? 'female' : 'male'}
                                                      color={(this.props.TipCameraMF === "F") ? 'pink' : (this.props.TipCameraMF === "M" ? 'blue' : 'grey')}
                                                      style={{transform: "scale(1.3)"}}/>}>
                                    <Dropdown.Menu>
                                        <Dropdown.Item onClick={this.schimbaGenulCamerei}>
                                            <Icon name={'male'} color={'grey'}
                                                  style={{transform: "scale(1.3)"}}/>
                                            Nespecificat
                                        </Dropdown.Item>
                                        <Dropdown.Item onClick={this.schimbaGenulCamerei}>
                                            <Icon name={'female'} color={'pink'}
                                                  style={{transform: "scale(1.3)"}}/>
                                            Fete
                                        </Dropdown.Item>
                                        <Dropdown.Item onClick={this.schimbaGenulCamerei}>
                                            <Icon name={'male'} color={'blue'}
                                                  style={{transform: "scale(1.3)"}}/>
                                            Băieți
                                        </Dropdown.Item>
                                    </Dropdown.Menu></Dropdown>
                                    <Icon name={(this.props.Disponibila === true)?'check circle outline':'times circle outline'} color={(this.props.Disponibila === true)?'green':'grey'} style={{marginLeft: "1px"}}/>
                                <Dropdown
                                    trigger={(this.props.Disponibila === true) ? "Disponibil" : "Indisponibil"}
                                    floating
                                    labeled
                                    button
                                >
                                    <Dropdown.Menu>
                                    <Dropdown.Item
                                        onClick={this.disponibilitateCamera}>
                                        {(this.props.Disponibila === true) ? "Indisponibil" : "Disponibil"}
                                    </Dropdown.Item>
                                    </Dropdown.Menu>
                                </Dropdown>
                            </Table.HeaderCell>
                            <Table.HeaderCell style={{width: "115px"}}>
                                {this.props.FacultatiCamera!==undefined&&
                                this.props.FacultatiCamera
                                    .map(item=> {
                                        (item.ID_Camera === this.props.ID_Camera) && (
                                            locuriTotal = parseInt(locuriTotal) + parseInt(item.NrLocuriAlocateFacultate))

                                    })}

                                <Popup flowing style={style2} trigger={<Icon name={'add user'} style={{
                                    marginRight: "10px",
                                    paddingBottom: "25px",
                                    paddingTop: "2px",
                                    color: "#3399CC",
                                    transform: "scale(1.3)"
                                }}/>} on='click'>
                                    <Grid centered divided columns={2}>
                                        <Grid.Column textAlign='center'>
                                            <Input style={{width: "99%"}} type="number" min={1} max={4} focus placeholder="Modificați nr. de locuri"
                                                   onChange={(event) => this.setState({nrLocuriCamera: event.target.value})}/>
                                        </Grid.Column>
                                        <Grid.Column textAlign='center'>
                                            <Button type="button" color={'green'} content={'Salvați modificările'} onClick={()=>this.adaugaNrLocuri(locuriTotal)}/>
                                        </Grid.Column>
                                    </Grid>
                                </Popup>
                                {this.props.NrLocuri <=0 ? "Locuri: 0" : "Locuri: " + locuriTotal + "/" + this.props.NrLocuri}
                            </Table.HeaderCell>
                            <Table.HeaderCell style={{width: "430px"}}>
                               {this.props.FacultatiCamera !== undefined &&
                            this.props.FacultatiCamera.map(item=> { //it shows the faculties accepted for this room or "camera neatribuita" if it's not selected yet
                                if (item.ID_Facultate !==-1 && this.props.FacultatiCamera.length>=1){
                                    return (
                                    <p key={item.ID_FacultateCamera} style={{padding: "0", margin: "0", lineHeight: "15px", display: "inline"}}>
                                        {item.DenumireFacultate}
                                        {item.DenumireFacultate != null &&
                                        <span style={{color: "green"}}> ({item.NrLocuriAlocateFacultate})</span>
                                        }
                                        {item.DenumireFacultate != null&&
                                            <Popup content={'Șterge facultate'}
                                            trigger={<Icon name={'trash alternate'} color={'red'}
                                                           onClick={() => this.stergeFacultate(item.ID_FacultateCamera)}/>}/>
                                        }

                                        </p>
                                )} else return("Neatribuită")})}
                            </Table.HeaderCell>
                            <Table.HeaderCell style={{width: "170px"}}>
                                <Popup flowing style={style}
                                       open={popupOpen}
                                        onOpen={this.openPopup}
                                       trigger={<Button type="button" style={{backgroundColor:"#bfd5d6"}} disabled={this.props.NrLocuri <= 0}>
                                           Adaugă facultate </Button>}
                                       on='click'>
                                    <Grid centered divided columns={3}>
                                        <Grid.Column textAlign='center'>
                                            <Input style={{width: "95%"}} type="number" min={1} focus placeholder="Număr locuri"
                                                   onChange={(event) => this.setState({
                                                       nrLocuriAlocate: event.target.value,
                                                       nrLocuriFacultatiCamera: locuriTotal
                                                   })}/></Grid.Column>
                                        <Grid.Column textAlign='center'>
                                            <Select placeholder={'Selectati facultatea'} options={this.props.listaFacultati}
    onChange={this.alegeFacultatea}/></Grid.Column>
                                        <Grid.Column textAlign='center'>
                                            <Button type="button" color='green' content='Adaugă facultate' onClick={this.facultateCameraAdd}/></Grid.Column>
                                    </Grid>
                                </Popup>
                            </Table.HeaderCell>
                        </Table.Row>
                    </Table.Header>

                </Table>
            </div>
        )
    }
}

export default CameraTabel;


