import axios from 'axios'

const instance = axios.create({
    baseURL: "http://dnndev.me/api/agsis.api/",
    headers: {'content-type': 'application/json;charset=UTF-8',}
});
export default instance;